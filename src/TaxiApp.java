import java.io.File;
import java.io.PrintWriter;
import java.util.PriorityQueue;

/**
 * TaxiApp uses a combination of A* and beam search to find optimum paths between a client and a taxi from a list of taxis.
 * Input:
 *      A file with the coordinates of the roads of our "world" in the form X,Y,roadId,roadName. RoadName is optional.
 *      The first line of the file should be "X,Y,roadId,roadName".
 *      A file with the coordinates of the taxis in the form X,Y,taxiId. The first line of the file should be "X,Y,taxiId".
 *      A file with the coordinates of the client in the form X,Y. The first line of the file should be "X,Y".
 *      A number in the range of Long numbers with the desired beam for the A* algorithm.
 *      Two file paths for the output files.
 * Output:
 *      A .out file in the form "Taxi" $taxiId$ "Cost" $cost$ \n $list of path coordinates of the optimum path for this
 *      taxi and beamNo$. This form is repeated for each of the taxis in the input file. If no path is found for a taxi
 *      instead of a coordinates list the program writes "Couldn't reach client with taxi " taxiId. The paths are given
 *      ordered by cost with the exception of the best path which is always printed last.
 *      A .kml file which should visualize the paths if given to the MyMaps site.
 *      The optimum path is green and the rest are grey.
 */

public class TaxiApp {
    public static void main(String[] args) {
        long startTimer = System.nanoTime();
        if(args.length != 6) {
            System.out.println("Correct usage is: java TaxiApp nodes.csv taxis.csv client.csv " +
                    "beamNo outputFileName kmlFilename");
            System.exit(1);
        }
        try {
            final File kmlFile = new File(args[5]);
            final File file = new File(args[4]);
            if(!file.exists()) {
                if(!file.createNewFile()) {
                    throw new java.io.IOException();
                }
            }
            if (!kmlFile.exists()) {
                if(!kmlFile.createNewFile()) {
                    throw new java.io.IOException();
                }
            }
            final PrintWriter writer = new PrintWriter(file);
            final PrintWriter kmlWriter = new PrintWriter(kmlFile);
            final SMAGraph graph = new SMAGraph(args[0]);               //Create an SMAGraph (adjacency table) from the
                                                                        //input file containing the map coordinates.
            final Node client = new Node(args[2]);
            final PriorityQueue<Route> routes =                                  //A container for each taxi's best path
                    new PriorityQueue<>(11, new RoutesComparator());//sorted by cost ascending

            for(final Node taxi : new NodeList(args[1])) {      //Create a list of Taxi Nodes and use A* on each of them
                routes.add(graph.SMA(taxi, client, Integer.parseInt(args[3])));
            }

            kmlFileInit(kmlWriter);
            Route firstRoute = new Route(Double.MAX_VALUE, null, -1, 0, 0);
            if(!routes.isEmpty()) {
                firstRoute = routes.poll();     //fastest route separated to assign a
            }                                   // different colour to it in the kml file
            while(!routes.isEmpty()) {
                Route route = routes.poll();
                route.print(writer, kmlWriter, false);  //print all paths except first as grey
            }
            firstRoute.print(writer, kmlWriter, true);  //print best path as green
            kmlFIleEnd(kmlWriter);
            kmlWriter.close();
            writer.close();
            long endTimer = System.nanoTime();
            System.out.println(endTimer-startTimer);
        } catch(java.io.FileNotFoundException e) {
            System.out.println("Cannot create output file or kml file");
            System.exit(1);
        } catch(java.io.UnsupportedEncodingException e) {
            System.out.println("UTF-8 encoding is not supported");
            System.exit(1);
        } catch(java.io.IOException e) {
            System.out.println("Couldn't create output file or kml file");
            System.exit(1);
        }

    }

    /**
     * Initialises the .kml output file properly, defining the styles used.
     * @param kmlWriter the writer for the .kml file.
     */

    private static void kmlFileInit(PrintWriter kmlWriter) {
        kmlWriter.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        kmlWriter.println("\t<kml xmlns=\"http://earth.google.com/kml/2.1\">");
        kmlWriter.println("\t<Document>");
        kmlWriter.println("\t\t<name>Taxi Routes</name>");
        kmlWriter.println("\t\t<Style id=\"green\">");
        kmlWriter.println("\t\t\t<LineStyle>");
        kmlWriter.println("\t\t\t\t<color>ff009900</color>");
        kmlWriter.println("\t\t\t\t<width>4</width>");
        kmlWriter.println("\t\t\t</LineStyle>");
        kmlWriter.println("\t\t</Style>");
        kmlWriter.println("\t\t<Style id=\"grey\">");
        kmlWriter.println("\t\t\t<LineStyle>");
        kmlWriter.println("\t\t\t\t<color>ffC3C3C3</color>");
        kmlWriter.println("\t\t\t\t<width>4</width>");
        kmlWriter.println("\t\t\t</LineStyle>");
        kmlWriter.println("\t\t</Style>");
    }

    /**
     * Closes the Document and kml blocks in the .kml output file.
     * @param kmlWriter the writer of the .kml file.
     */
    private static void kmlFIleEnd(PrintWriter kmlWriter) {
        kmlWriter.println("\t</Document>");
        kmlWriter.println("</kml>");
    }
}
