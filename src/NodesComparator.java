import java.util.Comparator;

/**
 * Implements a comparator for the Node class. The comparator is used by the
 * PriorityQueue class in order to sort Nodes by F ascending.
 */

class NodesComparator implements  Comparator<Node> {

    public int compare(Node n1, Node n2) {
        if(n1.getF() > n2.getF()) {
            return 1;
        }
        if(n2.getF() > n1.getF()) {
            return -1;
        }
        return 0;
    }
}
