import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.io.File;

/**
 * NodeList extends the class LinkedList and is purely implemented to add a constructor with a file path as its argument.
 */

class NodeList extends LinkedList<Node> {

    /**
     * Used to create a list of taxi Nodes.
     * @param filename filename is assumed to be the file path of the file containing taxi coordinates and taxiId's.
     *                 The first line of the file should be "X,Y".
     */

    NodeList(String filename) {
        try {
            final File file = new File(filename);
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            reader.readLine();
            String text;

            while((text = reader.readLine()) != null) {
                final String[] arr = text.split(",");
                if(arr.length != 3) {
                    System.out.println("Taxis file is not in the expected form. " +
                            "Taxis file should consist of lines in the form: X Y taxiId.");
                    System.exit(2);
                }
                this.add(new Node(arr[0], arr[1], Integer.parseInt(arr[2])));
            }
            reader.close();
        } catch(java.io.FileNotFoundException e) {
            System.out.println("File of taxis not found");
            System.exit(2);
        } catch(java.io.IOException e) {
            System.out.println("IO error occurred in taxis list");
            System.exit(2);
        }
    }
}
