import java.io.PrintWriter;
import java.util.LinkedList;

/**
 * The Route class implements a path from a taxi to the client. It stores the path as a LinkedList of Nodes,
 * the cost in kilometers of this path as a double and the taxiId.
 */
class Route {
    private final double cost;
    private final LinkedList<Node> path;
    private final Integer taxiId;
    private final int noOfIterations;
    private final int maxFringeSize;

    Route(double cost, LinkedList<Node> path, Integer taxiId, int noOfIterations, int maxFringeSize) {
        this.cost = cost;
        this.path = path;
        this.taxiId = taxiId;
        this.noOfIterations = noOfIterations;
        this.maxFringeSize = maxFringeSize;
    }

    double getCost() {
        return cost;
    }

    /**
     * The method print is used to output the path in both the .out and .kml files.
     * writer writes at the .out file and kmlWriter writes at the .kml file. The isFirst variable determines
     * the colour of the path in the .kml file. When it is set to true the path is green, else it is grey.
     */

    void print(PrintWriter writer, PrintWriter kmlWriter, boolean isFirst) {
        if(writer == null || kmlWriter == null) {
            throw new NullPointerException("Attempted to write a path with a null writer");
        }
        if(path == null) {
            writer.println("Couldn't reach client with taxi " + taxiId);
            return;
        }
        writer.println("Taxi " + taxiId + " Cost " + cost + " Number of Iterations " + noOfIterations + " Max fringe" +
                " size " + maxFringeSize);
        kmlWriter.println("\t\t<Placemark>");
        kmlWriter.println("\t\t\t<name>Taxi " + taxiId + "</name>");
        if (isFirst) {
            kmlWriter.println("\t\t\t<styleUrl>#green</styleUrl>");
        } else {
            kmlWriter.println("\t\t\t<styleUrl>#grey</styleUrl>");
        }
        kmlWriter.println("\t\t\t<LineString>");
        kmlWriter.println("\t\t\t\t<altitudeMode>relative</altitudeMode>");
        kmlWriter.println("\t\t\t\t<coordinates>");
        while(!path.isEmpty()) {
            final Node node = path.poll();
            writer.print(node.getKey() + " ");
            kmlWriter.println("\t\t\t\t\t" + node.getKey() + ",0");
        }
        kmlWriter.println("\t\t\t\t</coordinates>");
        kmlWriter.println("\t\t\t</LineString>");
        kmlWriter.println("\t\t</Placemark>");
        writer.println();
    }
}
