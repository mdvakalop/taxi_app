import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * The class Node implements a point on the map traversed by the A* algorithm.
 * It stores the X and Y coordinates as doubles,
 * a string, key == X + "," + Y which is used to output the exact coordinates as given in the input files and
 * to differentiate between Nodes. Nodes with the same key and differences in some other field
 * are to be considered the same by the method equals().
 * an int, hash, which is the output of the method key.hashCode(),
 * the doubles F,G which represent the F,G functions as described by most typical A* implementations,
 * a Long, id, which is used by map points to save their roadId, and the taxis to save their taxiId.
 */

class Node {
    private final String key;
    private final int hash;     //The hash is stored separately from the key and is not calculated each time hashCode()
                                //is called. This saves time by using ints instead of Strings with a small cost in memory.
    private final double x;
    private final double y;
    private double F, G;        //F is the total cost for the current node (heuristic h + distance g).
                                //G is the distance from starting point to current.
                                //As h we assume the Euclidean distance from current node to target
    private final Integer id;

    String getKey() {
        return key;
    }

    double getF() {
        return F;
    }

    Integer getId() {
        return id;
    }

    void setG(double G) {
        this.G = G;
    }

    /**
     * This constructor is called by SMAGraph and NodeList to create the Nodes for the taxis and the map.
     */

    Node(String x, String  y, Integer id) {
        this.key = x + "," + y;
        this.hash = this.key.hashCode();
        this.x = Math.toRadians(Double.parseDouble(x));
        this.y = Math.toRadians(Double.parseDouble(y));
        this.id = id;
    }

    /**
     * This constructor is to be used to create the Node representing the client.
     * @param filename should be the file path containing the information of the client.
     *                 The first line of the file should be "X,Y".
     */

    Node(String filename) {
        String localKey = null;
        int localHash = -1;
        double localX = -1;
        double localY = -1;
        Integer localId = null;
        try {
            final File file = new File(filename);
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            reader.readLine();      //First line contains junk.
            final String text = reader.readLine();
            final String[] arr = text.split(",");
            if(arr.length != 2) {
                System.out.println("Client file is not in the expected form. " +
                        "Client file should contain a line in the form: X Y.");
                System.exit(3);
            }
            localKey = arr[0] + "," + arr[1];
            localHash = localKey.hashCode();
            localX = Math.toRadians(Double.parseDouble(arr[0]));
            localY = Math.toRadians(Double.parseDouble(arr[1]));
            localId = -1;
            reader.close();

        } catch(java.io.IOException e) {
            System.out.println("IO error occurred in client");
            System.exit(3);
        }
        this.key = localKey;
        this.hash = localHash;
        this.x = localX;
        this.y = localY;
        this.id = localId;

    }

    /**
     * Overrides the method Object.equals(Object obj).
     * @param obj Object to be compared with caller.
     * @return Returns true when the key fields of the caller and the parameter Object are equal.
     * If the obj argument is null or of a different class this method always returns false.
     */

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        Node other = (Node) obj;
        return this.key.equals(other.key);
    }

    /**
     * Overrides the method Object.hashCode()
     * @return returns the output of the method String.hashCode() for the object field "key".
     * The result of this method has already been stored in the field "hash" in object creation time.
     * This is done to reduce computational time.
     */

    @Override
    public int hashCode() {
        return hash;
    }

    /**
     * Updates the F field of the caller. Calculates The G parameter of the A* algorithm as the distance from taxi to
     * currentNode plus the distance from currentNode to this. If this "newG" is smaller than the previous value of the
     * field  G this path is the shortest one discovered so far. In this case F and G are updated. Otherwise we have
     * already discovered a shorter path to this Node. F and G are left the same. All distances are calculated assuming
     * the map is approximately a plane.
     * @param currentNode a Node which is assumed to be a neighbour of this Node and the previous
     *                    Node in the path we are currently exploring.
     * @param client a Node which is assumed to be the target of the A* algorithm. Its coordinates as used to calculate
     *               the heuristic. The heuristic is simply the Euclidean distance between this Node and the client.
     * @return Returns true when F and G are updated. Else returns false.
     */

    boolean updateF(Node currentNode, Node client) {
        final double newG = calculateDistance(currentNode) + currentNode.G;
        //final double newG = Math.sqrt((currentNode.x - x)*(currentNode.x - x) + (currentNode.y - y)*(currentNode.y - y)) + currentNode.G;
        final boolean b = newG < G;
        if(b) {
            G = newG;
            final double H = calculateDistance(client);
            //final double H = Math.sqrt((client.x - x) * (client.x - x) + (client.y - y) * (client.y - y));
            F = G + H;
        }
        return b;
    }

    /**
     * Calculates the distance between this Node and Node n. It's used by the SMAGraph.addNode() method to find
     * the closest Node already in the graph to a given Node. This is used to add the taxi and client Nodes to the graph
     * and connect them with their closest neighbours. The distance is calculated assuming the map is approximately a plane.
     * @param n The Node from which the distance is calculated.
     * @return Returns the distance between the Nodes as a double.
     */

    double calculateDistance(Node n) {
        final double dy = y - n.y;
        final double dx = (x - n.x)*Math.cos(n.y);
        return 6371.01 * Math.sqrt(dx*dx + dy*dy);
        /*final int R = 6371; // Radius of the earth

        final double latDistance = Math.toRadians(n.y - y);
        final double lonDistance = Math.toRadians(n.x - x);
        final double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(n.y)) * Math.cos(Math.toRadians(y))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        final double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c; // convert to meters*/
        //return Math.sqrt((n.x - x) * (n.x - x) + (n.y - y) * (n.y - y));
    }
}
