import java.util.PriorityQueue;

class RoutesQueue extends PriorityQueue<Route> {

    RoutesQueue(int n, RoutesComparator comp) {
        super(n, comp);
    }
}
