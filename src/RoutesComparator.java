import java.util.Comparator;

/**
 * Implements a comparator for the Route class. The comparator is used by the
 * PriorityQueue class in order to sort paths by cost ascending.
 */
class RoutesComparator implements Comparator<Route> {

    public int compare(Route r1, Route r2) {
        if(r1.getCost() > r2.getCost()) {
            return 1;
        }
        if(r2.getCost() > r1.getCost()) {
            return -1;
        }
        return 0;
    }
}
