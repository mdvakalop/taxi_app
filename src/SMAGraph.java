//import org.jetbrains.annotations.NotNull;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

/**
 * SMAGraph implements the map as a graph in the form af and adjacency matrix. Each Node is mapped to a LinkedList of its
 * neighbouring Nodes using a HashMap. Each road in the map is stored as a mapping from roadId to a LinkedList of Nodes
 * representing all the point of the map on that road.
 */

class SMAGraph {
    private final HashMap<Integer, LinkedList<Node>> roads =  new HashMap<>();
    private final HashMap<Node, LinkedList<Node>> graph = new HashMap<>();

    /**
     * This constructor is to be used to create the Nodes of the map and stores them in the HashMap roads grouped in roads
     * and in the HashMap graph, mapping each Node to its neighbours.
     * @param filename filename is assumed to be the file path of the file containing the coordinates of the map Nodes
     *                 and their roadId's in the form X,Y,roadId,roadName. RoadName is optional.
     *                 The first line of the file should be "X,Y,roadId,roadName". All the Nodes of each road MUST be
     *                 given continuously and in order.
     */

    SMAGraph(String filename) {
        try {
            final File file = new File(filename);
            final BufferedReader reader = new BufferedReader(new FileReader(file));
            reader.readLine();      //First line contains junk.
            String text;
            Integer oldRoadId = -1;

            while((text = reader.readLine()) != null) {
                final String[] arr = text.split(",");
                if(arr.length < 3) {
                    System.out.println("Map file is not in the expected form. Map file should consist of lines in the form: " +
                            "X,Y,roadId,roadName. RoadName is optional");
                    System.exit(4);
                }

                final int roadId = Integer.parseInt(arr[2]);
                final Node node = new Node(arr[0], arr[1], roadId);

                if(!oldRoadId.equals(roadId)) {            //If this road hasn't already been encountered, create the
                    roads.put(roadId, new LinkedList<>());  //LinkedList representing it and put it in the HashMap.
                }
                roads.get(roadId).addLast(node);

                if(!graph.containsKey(node)) {              //Nodes with the same hash are considered to be the same.
                    graph.put(node, new LinkedList<>());    //This way the LinkedList of a Node on an intersection
                    //contains neighbouring Nodes from every road it is on.
                }
                oldRoadId = roadId;
            }
            reader.close();
            Collection<LinkedList<Node>> roadsCollection = roads.values();
            for (final LinkedList<Node> road : roadsCollection) {    //roadsCollection contains all of the roads on the given map.
                for (final ListIterator<Node> nodeIt = road.listIterator(1); nodeIt.hasNext(); ) {
                    final Node n1 = nodeIt.previous();      //A Node is assumed to be neighbors with the previous and
                    nodeIt.next();                          //next Node as ordered in the input file.
                    final Node n2 = nodeIt.next();
                    addEdge(n1, n2);

                }
            }


        } catch(java.io.FileNotFoundException e) {
            System.out.println("File of taxis not found");
            System.exit(4);
        } catch(java.io.IOException e) {
            System.out.println("IO error occurred in taxis list");
            System.exit(4);
        }
    }

    /**
     * This method is used to add a taxi and the client in the graph by finding their nearest Node already in the graph
     * and connecting them with an edge.
     * @param nodeToAdd nodeToAdd is either the taxi or the client.
     */

    private void addNode(Node nodeToAdd) {
        if(nodeToAdd == null) {
            throw new NullPointerException("Attempted to add a null Node to the graph");
        }
        double minDistance = Double.MAX_VALUE;
        Node minNode = new Node("-1.0", "-1.0", -1);
        for(final LinkedList<Node> list : roads.values()) {
            for(final Node node : list) {
                if(node.calculateDistance(nodeToAdd) < minDistance) {
                    minDistance = node.calculateDistance(nodeToAdd);
                    minNode = node;
                }
            }
        }
        graph.put(nodeToAdd, new LinkedList<>());
        addEdge(nodeToAdd, minNode);
    }

    private void removeNode(Node node) {
        if(node == null) {
            throw new NullPointerException("Attempted to remove a null Node from the graph");
        }
        graph.remove(node);
    }

    /**
     *This method adds Node n2 as a neighbour to Node n1 and vice versa by adding each Node to the other's adjacency
     * LinkedList in the HashMap.
     */

    private void addEdge(Node n1, Node n2) {
        if(n1 == null || n2 == null) {
            throw new NullPointerException("Attempted to connect null Nodes with an edge");
        }
        graph.get(n1).add(n2);
        graph.get(n2).add(n1);
    }

    /**
     * SMA implements an A* search algorithm with a bounded fringe size. This algorithm is called Simple Memory-Bounded
     * A* in some bibliography. The fringe of the algorithm is implemented as a priority queue which always expands the
     * Node with the smallest F cost and adds it to the closed set. Every time we expand a Node its neighbours are added
     * to the fringe if they haven't been visited yet or the newly discovered path is shorter than the already existing
     * one. After visiting the Node's neighbours if the fringe size is bigger than beamNo we create a new queue and
     * insert the best $beamNo$ Nodes.
     * @param taxi is the starting Node of the algorithm.
     * @param client is the goal Node of the algorithm.
     * @param beamNo is the fringe size limit.
     * @return Returns a Route containing the information of the best path between taxi and client.
     */

    Route SMA(Node taxi, Node client, int beamNo) {
        for(final LinkedList<Node> road : roads.values()) {
            for(final Node node : road) {
                node.setG(Double.MAX_VALUE);
            }
        }
        //PriorityQueue<Node> fringe =
        //new PriorityQueue<>(11, new NodesComparator()); //Contains the Nodes
        final TreeSet<Node> fringe = new TreeSet<>(new NodesComparator());
        taxi.setG(0.0);
        addNode(taxi);
        addNode(client);
        client.setG(Double.MAX_VALUE);      //initialising the G value of all Nodes in graph as "infinite".
        fringe.add(taxi);

        final Set<Node> closedSet = new HashSet<>();        //Set of Nodes already visited.
        final HashMap<Node, Node> prev = new HashMap<>();   //prev.get(currentNode) returns the Node visited by A* before
        //currentNode in the best path to currentNode, found so far.
        int noOfIterations = 0;
        int maxFringeSize = 0;

        while(!fringe.isEmpty()) {
            noOfIterations++;
            final Node currentNode = fringe.pollFirst();

            if(currentNode.equals(client)) {
                //System.out.println("Found client");
                removeNode(taxi);
                removeNode(client);
                return calculatePath(client, taxi, prev, noOfIterations, maxFringeSize);
            }

            closedSet.add(currentNode);

            for(final Node neighbour : graph.get(currentNode)) {
                if(!closedSet.contains(neighbour)) {
                    if(neighbour.updateF(currentNode, client)) {    //Don't make changes to queue if you have already
                        //visited the node with a shorter path.
                        if(fringe.contains(neighbour)) {            //Java doesn't support priority updates.
                            fringe.remove(neighbour);               //The priority of a Node has possibly just changed.
                        }                                           //We need to remove it and reinsert it.
                        fringe.add(neighbour);
                        prev.put(neighbour, currentNode);
                    }
                }
            }

            while(fringe.size() > beamNo) {     //If the size of fringe has exceeded beam width only keep the best
                //$beamNo$ Nodes
                fringe.pollLast();
                /*PriorityQueue<Node> newFringe = new PriorityQueue<>(11, new NodesComparator());
                for(int i = 0; i < beamNo; i++) {
                    newFringe.add(fringe.poll());
                }
                fringe = newFringe;*/
            }
            if(fringe.size() > maxFringeSize) {
                maxFringeSize = fringe.size();
            }
        }
        removeNode(taxi);
        removeNode(client);
        //System.out.println("Taxi " + taxi.getId() + " couldn't reach the client with this amount of memory");
        return new Route(Integer.MAX_VALUE, null, taxi.getId(), noOfIterations, maxFringeSize);
    }

    /**
     * Reconstructs
     * the optimal path between taxi and client as found by the SMA method.
     * @param client The goal of the A* algorithm.
     * @param taxi The starting position of the A* algorithm.
     * @param prev prev is assumed to contain the previous Node of each Node visited in an optimal path.
     * @return Returns a Route object containing the cost in kilometers of the optimal path, the optimal path
     *          as a list of Nodes and the id of the specific taxi.
     */

    //@NotNull
    private Route calculatePath(Node client, Node taxi, HashMap<Node, Node> prev,
                                int noOfIterations, int maxFringeSize) {
        final LinkedList<Node> path = new LinkedList<>();
        Node currentNode = client;
        while(currentNode != taxi) {
            path.addFirst(currentNode);
            currentNode = prev.get(currentNode);
        }
        path.addFirst(taxi);
        return new Route(client.getF(), path, taxi.getId(), noOfIterations, maxFringeSize);
    }
}
